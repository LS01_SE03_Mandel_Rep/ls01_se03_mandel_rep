import java.util.Comparator;

public class SortBenutzernummer implements Comparator<Benutzer>{
	@Override
	public int compare(Benutzer b1, Benutzer b2) {
		return b1.getBenutzernummer() - b2.getBenutzernummer();
	}
}
