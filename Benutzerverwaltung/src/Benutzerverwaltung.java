import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Benutzerverwaltung {
	
	private static ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();

	public static void main(String[] args) {
		
		
		int auswahl;
		
		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen();
				break;
			case 2:
				benutzerErfassen();
				break;
			case 3:
				benutzerL�schen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");				
			}
		}while (true );
		
	}
	
	public static void benutzerAnzeigen() {
	    
		for (int i=0 ; i < benutzerliste.size(); i++) {
			System.out.println(benutzerliste.get(i).toString());
//			System.out.println(benutzerliste.get(i).getName());
//			System.out.println(benutzerliste.get(i).getBenutzernummer());
			
		}
	}
	
	public static void benutzerErfassen() {
		
		Benutzer b = new Benutzer("", 0);

		System.out.println("Name: ");
		Scanner s = new Scanner(System.in);
		String name = s.next();
		b.setName(name);
		
		System.out.println("Benutzernummer: ");
		int benutzernummer = s.nextInt();
		b.setBenutzernummer(benutzernummer);	
		
		benutzerliste.add(new Benutzer (name, benutzernummer));
		Collections.sort(benutzerliste, new SortBenutzernummer());
		
	}		
	
	
	public static void benutzerL�schen() {
		
		System.out.println("Hier ist das aktuelle Benutzerverzeichnis: \n ");
		benutzerAnzeigen();
		
		System.out.println("\nZum L�schen eines Benutzer's geben Sie die gew�nschten Eintragsnummer an. \n" + "Der erste Benutzer hat immer die Eintragsnummer '0'! \n" + "W�hlen Sie die Eintragsnummer: ");
		Scanner s = new Scanner(System.in);
		int eintrag = s.nextInt();
		
		if (eintrag != benutzerliste.size()) {
			benutzerliste.remove(eintrag);
			System.out.println("Der Eintrag wurde erfolgreich gel�scht!\n");
		} else {
			System.err.println("Dieser Eintrag exisitiert nicht, versuchen Sie es sp�ter mit einer anderen Eintragsnummer erneut.\n");
		}
	}
	
	
	public static int menu() {

        int selection;
        Scanner input = new Scanner(System.in);

        /***************************************************/

        System.out.println("     Benutzerverwaltung     ");
        System.out.println("----------------------------\n");
        System.out.println("1 - Benutzer anzeigen");
        System.out.println("2 - Benutzer erfassen");
        System.out.println("3 - Benutzer l�schen");
        System.out.println("4 - Ende");
  
        System.out.print("\nEingabe:  ");
        selection = input.nextInt();
        return selection;    
    }

}
