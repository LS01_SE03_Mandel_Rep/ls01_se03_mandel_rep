
public class MethodeBeispiel {

	public static void main(String[] args) {
	
		
		//sayHello("Max");
		//sayHello();
		//add (7,10);
		//addDouble (3.5, 7.7);
		//summe(3,6,7);
		
		berechnePreis(3, 10.5, "Monitore");
	}
	public static void berechnePreis(int zahl1, double zahl2, String name) {
		double erg = zahl1 * zahl2;
	
		System.out.printf("3 "+ name+ " kosten "+ "%.2f" + " Euro", erg);
	}
	
	public static void summe(int zahl1,int zahl2,int zahl3) {
		int erg = zahl1 + zahl2 + zahl3;
		System.out.println(zahl1 + " + "+zahl2 + " + "+zahl3 + " = " + erg);		
	}
	
	
	
	// addDouble hei�t die Methode
	public static void addDouble(double zahl1, double zahl2) {
		double erg = zahl1 + zahl2;
		System.out.println("Zahl1: " + zahl1);
		System.out.println("Zahl2: " + zahl2);
		System.out.println("Ergebnis: " + erg);
	}
	
	public static void add(int zahl1, int zahl2) {
		int erg = zahl1 + zahl2;
		System.out.println(zahl1 + "+"+zahl2 + " = "+ erg);
	}
	
	
	// Void bedeutet kein R�ckgabewert
	public static void sayHello() {
		System.out.println("hello ...");
	}
	
	// Das "String name" steht f�r Parameter
	public static void sayHello(String name) {
		System.out.println("hello "+name);
	}
	
}
