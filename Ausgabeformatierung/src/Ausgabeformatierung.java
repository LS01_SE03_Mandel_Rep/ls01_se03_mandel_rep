
public class Ausgabeformatierung {

	public static void main(String[] args) {
		
/*		System.out.print("Hello\n");
	
		System.out.println("Hello");
		
		System.out.println("Preis:\t"+ 2.90 + " Euro");
*/

	    //String
		
//		System.out.printf("%s", "123456789\n");
//		System.out.printf("|%-20s|", "123456789");
//		System.out.printf("\n|%20s|%n", "123456789");
//		System.out.printf("|  %-20s|", "123456789");
	
		//Ganzzahl
		
//		System.out.printf("|%20d|%n", 123456789);
//		System.out.printf("|%-20d|%n", 123456789);
//		System.out.printf("|%020d|%n", 123456789);
		
		// Kommazahl
		
//		System.out.printf("|%f|%n", 12345.6789);
//		System.out.printf("|%.2f|%n", 12345.6789);
//		System.out.printf("|%20.2f|%n", 12345.6789);
//		System.out.printf("|%-20.2f|%n", 12345.6789);
		
	System.out.printf("Monitor : %.2f Euro %nTastatur: %.2f Euro %n", 103.45, 45.67);	
	}

}
