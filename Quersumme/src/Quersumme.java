import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {

		int Zahl = 0;
		int Quersumme = 0;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.printf("Zahl eingeben: ");
		Zahl = tastatur.nextInt();
		
		tastatur.close();
		
		while (Zahl != 0) {
			Quersumme = Quersumme + Zahl % 10;
			Zahl = Zahl / 10;
		}
		System.out.printf("Die Quersumme lautet %d", Quersumme);

	}

}
