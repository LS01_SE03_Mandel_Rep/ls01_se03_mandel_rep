
public class MethodeBeispiel2 {

	public static void main(String[] args) {
		
		int erg = add(3,4);
		System.out.println("Ergebnis : " + erg);
		
		int erg2 = summe(3,4,5);
		System.out.println("Ergebnis : " + erg2);
		
		double x = 2.0;
		double y = 4.0;
		double m = berechneMittelwert(x, y);		
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}

	public static int add(int zahl1, int zahl2) {
		int erg = zahl1 + zahl2;
		//System.out.println(zahl1 + " + "+ zahl2 +" = "+ erg);
		
		return erg;
		
	}
	
	public static int summe(int zahl1, int zahl2, int zahl3) {
		int erg2 = zahl1 + zahl2 + zahl3;
		
		return erg2;
	}
    
	public static double berechneMittelwert(double x, double y) {
		double m = (x + y) / 2.0;
		return m;
		
	}
	
}
