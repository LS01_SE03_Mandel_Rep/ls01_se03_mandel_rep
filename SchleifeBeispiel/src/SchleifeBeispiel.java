import java.util.Scanner;

public class SchleifeBeispiel {

	public static void main(String[] args) {
		
		System.out.printf("Bitte geben Sie eine Zahl ein");
		Scanner myScanner = new Scanner(System.in);
		int n = myScanner.nextInt();
		
		int zaehler = 1;
		
		while (zaehler <= n) {
			if (zaehler == n) {
				System.out.print(zaehler);
			}else {
				System.out.print(zaehler + ", ");
			}
			zaehler++;
		}
	}

}
