import java.util.Scanner;

public class Temperaturumrechnung {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.printf("Bitte den Startwert in Celsius eingeben: ");
		double SC = s.nextDouble();
		
		System.out.printf("Bitte den Endwert in Celsius eingeben: ");
		double EC = s.nextDouble();
		
		System.out.printf("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double SW = s.nextDouble();
		
		System.out.println();
		
		while(SC <= EC) {
			double fahrenheit = 0;
			fahrenheit = 32 + (SC * 1.8);
			if (SC == EC) {
				System.out.println(SC+" �C		" + fahrenheit + " �F");
			} 
			else {
				System.out.println(SC+" �C		"+fahrenheit + " �F");
			}
			SC = SC + SW;
		}
	}

}
