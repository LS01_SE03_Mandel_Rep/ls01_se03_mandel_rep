﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static double fahrkartenbestellungErfassen (String text) {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	double d = myScanner.nextDouble();
    	return d;
	}
	
	public static double fahrkartenBezahlen (double zuZahlenderBetrag, int anzahlFahrkarten) {
		double zuZahlen = zuZahlenderBetrag * anzahlFahrkarten;
    	return zuZahlen;
		
	}
	public static void fahrkartenAusgeben (String text) {
    	System.out.println(text);
    	
	}
    public static void rueckgeldAusgeben (String text) {
    	System.out.println(text);
    		
    }
    public static void warte(int millisekunde) {
    	for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    	System.out.println("\n\n");
    }
    public static void muenzeAusgeben(double betrag, String einheit) {
    	while(betrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 "+ einheit);
	          betrag -= 2.0;
        }
        while(betrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 "+ einheit);
	          betrag -= 1.0;
        }
        while(betrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("0,50 "+ einheit);
	          betrag -= 0.5;
        }
        while(betrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("0,20 "+ einheit);
	          betrag -= 0.2;
        }
        while(betrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("0,10 "+ einheit);
	          betrag -= 0.1;
        }
        while(betrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("0,05 "+ einheit);
	          betrag -= 0.05;
        }
    }
		
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       //double zuZahlenderBetrag;
       double zuZahlenderBetrag = fahrkartenbestellungErfassen("Zu zahlender Betrag (EURO): ");
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int anzahlFahrkarten;
       
       
       //System.out.print("Zu zahlender Betrag (EURO): ");
       //zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Anzahl der Fahrkarten: ");
       anzahlFahrkarten = tastatur.nextInt();
 
       //zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarten;
       double zuZahlen = fahrkartenBezahlen (zuZahlenderBetrag, anzahlFahrkarten);
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("%s%5.2f\n","Noch zu zahlen: ",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
       // Fahrscheinausgabe
       // -----------------;
       fahrkartenAusgeben("\nFahrschein wird ausgegeben");
       warte(1);

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   rueckgeldAusgeben("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO wird in folgenden Münzen ausgezahlt:");
    	   //System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   //System.out.println("wird in folgenden Münzen ausgezahlt:");
    	   muenzeAusgeben(rückgabebetrag,"EURO");
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}