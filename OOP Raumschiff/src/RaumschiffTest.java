public class RaumschiffTest {

	public static void main(String[] args) {
		
		//------------Raumschiff erstellen------------
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 1, 100, 100, 100, 100, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 0, 80, 80, 50, 100, 5);
		
		//------------Ladung erstellen-------------
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		//------------Ladung zuweisen--------------
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
// ------------- Testen der Raumschifffunktionen ----------------
		System.out.println("Testen der Raumschifffunktionen");
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedoSchiessen(vulkanier);
		klingonen.nachrichtAnAlle("Guten Abend");
		klingonen.eintraegeLogbuchZurueckgeben();
		
		
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		romulaner.photonentorpedoSchiessen(klingonen);
		romulaner.nachrichtAnAlle("Hallo");
		romulaner.eintraegeLogbuchZurueckgeben();
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		vulkanier.nachrichtAnAlle("Wir sind cool!");
		vulkanier.photonentorpedoSchiessen(klingonen);
		vulkanier.eintraegeLogbuchZurueckgeben();

		klingonen.zustandRaumschiff();
		klingonen.phaserkanoneSchiessen(vulkanier);
		vulkanier.zustandRaumschiff();
		klingonen.phaserkanoneSchiessen(vulkanier);
		vulkanier.zustandRaumschiff();
	}
	

}
