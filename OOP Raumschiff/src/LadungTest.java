
public class LadungTest {

	public static void main(String[] args) {
		
		Ladung l = new Ladung();
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
	
		System.out.println("Ladung: " + l.getBezeichnung() + "\n");
		
		l.setBezeichnung("Ladung - Fische");
		
		System.out.println("Ladung ver�ndert: " + l.getBezeichnung() + "\n");
		
		System.out.println("1.Ladung: " + l1.getBezeichnung());
		System.out.println("Ladungsmenge von 1.Ladung: " + l1.getMenge()  + "\n");
		
		System.out.println("2.Ladung: " + l2.getBezeichnung());
		System.out.println("Ladungsmenge von 2.Ladung: " + l2.getMenge()  + "\n");
		
		System.out.println("3.Ladung: " + l3.getBezeichnung());
		System.out.println("Ladungsmenge von 3.Ladung: " + l3.getMenge()  + "\n");
		
		System.out.println("4.Ladung: " + l4.getBezeichnung());
		System.out.println("Ladungsmenge von 4.Ladung: " + l4.getMenge()  + "\n");
		
		System.out.println("5.Ladung: " + l5.getBezeichnung());
		System.out.println("Ladungsmenge von 5.Ladung: " + l5.getMenge()  + "\n");
		
		System.out.println("6.Ladung: " + l6.getBezeichnung());
		System.out.println("Ladungsmenge von 6.Ladung: " + l6.getMenge()  + "\n");
		
		System.out.println("7.Ladung: " + l7.getBezeichnung());
		System.out.println("Ladungsmenge von 7.Ladung: " + l7.getMenge()  + "\n");
	}

}
