import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	public Raumschiff() {
		this.schiffsname = "Unbekannt";
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
	}
	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, 
	int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, 
	int lebenserhaltungssystemeInProzent, int androidenAnzahl) {
		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setSchiffsname (String schiffsname) {
		this.schiffsname = schiffsname;
	}	
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl (int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	public void setSchildeInProzent (int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzent (int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzent (int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl (int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	public void ladungsverzeichnisAusgeben() {
		for (int i=0 ; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i).toString());
	}
	}
	
	public void zustandRaumschiff() {
		System.out.println("\n" + "Raumschiff: " + getSchiffsname());
		System.out.println("Photonentorpedoanzahl: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent());
		System.out.println("Schilde: " + getSchildeInProzent());
		System.out.println("H�lle: " + getHuelleInProzent());
		System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent());
		System.out.println("Androidenanzahl: " + getAndroidenAnzahl());
	}
	
	public void nachrichtAnAlle(String message) {
	//	System.out.println("Vom Raumschiff " + getSchiffsname() + " kommt die Meldung: " + message);
		System.out.println(message);
		broadcastKommunikator.add(message);
	}
	
	public void photonentorpedoSchiessen (Raumschiff r) {
		
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("\nPhotonentorpedo abgeschossen");
			r.treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen (Raumschiff r) {
		
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("\n-=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("\nPhaserkanone abgeschossen");
			r.treffer(r);
		}
	}
	
	private void treffer (Raumschiff r) {
		System.out.println("[" + r.toString() + "] wurde getroffen!\n");
			schildeInProzent = schildeInProzent - 50;
			if (schildeInProzent <= 0) {
				schildeInProzent = 0;
				huelleInProzent = huelleInProzent - 50;
				energieversorgungInProzent = energieversorgungInProzent - 50;
				} if (huelleInProzent <= 0) {
					huelleInProzent = 0;
					energieversorgungInProzent = 0;
					lebenserhaltungssystemeInProzent = 0;
					System.out.println("Die Lebenserhaltungssysteme von [" + r.toString() + "] sind vernichtet worden!\n");
				}
	}
	
	public String toString() {
		return this.schiffsname; 
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		for (int i=0 ; i < broadcastKommunikator.size(); i++) {
			System.out.println(broadcastKommunikator.get(i).toString());
	}
		return broadcastKommunikator;
}
	
}