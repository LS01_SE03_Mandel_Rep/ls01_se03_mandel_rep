import java.util.Scanner;

public class PCHaendler {

    public static String liesString(String text) {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	String str = myScanner.next();
    	return str;
    }
    
    public static int liesInt(String text) {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	int i = myScanner.nextInt();
    	return i;
    	
    }
    
    public static double liesDouble(String text) {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	double d = myScanner.nextDouble();
    	return d;
    	
    }
    
    public static double liesMwst(String text) {
    	Scanner myScanner = new Scanner(System.in);
    	System.out.println(text);
    	double d = myScanner.nextDouble();
    	return d;
    }
	
    public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
    
    }
    
    public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
    	double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
    	return bruttogesamtpreis;
    
    }
    
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		//System.out.println("was m�chten Sie bestellen?");
		//String artikel = myScanner.next();
		String artikel = liesString("was m�chten Sie bestellen?");

		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();
		double mwst = liesMwst("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		double nettogesamtpreis = berechneGesamtnettopreis (anzahl, preis);
		
		double bruttogesamtpreis = berechneGesamtbruttopreis (nettogesamtpreis, mwst);
		
		
		
		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("Rechnung");
		System.out.printf("\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	 
	}

	}
